import time

from selenium.webdriver.common.by import By

from common.base import Base


class IndexPage(Base):
    def search_username(self):
        return self.get_element_text((By.CLASS_NAME, 'f4_b'))

    def logout(self):
        self.click((By.LINK_TEXT, '退出'))
        time.sleep(3)