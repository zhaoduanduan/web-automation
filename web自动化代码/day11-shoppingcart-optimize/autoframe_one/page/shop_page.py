import time
from selenium.webdriver.common.by import By
from common.base import Base
from common.functions import open_browser


class ShopPage(Base):
    # 把商品加入购物车
    def add_shop(self):
        # 聚焦
        self.exe_script((By.XPATH, '//a[1]/div[1]/img'))
        # 加购
        self.click((By.XPATH, '//a[1]/div[1]/img'))
        self.click((By.XPATH, '//td[@class="td1"]/a/img'))
        time.sleep(2)
        # 返回首页继续购物
        self.click((By.XPATH, '//a/img[@alt="continue"]'))
        time.sleep(2)
        # 聚焦
        self.exe_script((By.XPATH, '//a[2]/div[1]/img'))
        # 加购
        self.click((By.XPATH, '//a[2]/div[1]/img'))
        self.click((By.XPATH, '//td[@class="td1"]/a/img'))
        time.sleep(2)
        # 返回首页继续购物
        self.click((By.XPATH, '//a/img[@alt="continue"]'))
        time.sleep(2)
        # 聚焦
        self.exe_script((By.XPATH, '//a[7]/div[1]/img'))
        # 加购
        self.click((By.XPATH, '//a[7]/div[1]/img'))
        self.click((By.XPATH, '//td[@class="td1"]/a/img'))
        time.sleep(2)

        # 返回首页继续购物
        self.click((By.XPATH, '//a/img[@alt="continue"]'))
        time.sleep(2)
        # 聚焦
        self.exe_script((By.XPATH, '//a[6]/div[1]/img'))
        # 加购
        self.click((By.XPATH, '//a[6]/div[1]/img'))
        self.click((By.XPATH, '//td[@class="td1"]/a/img'))
        time.sleep(2)


if __name__ == '__main__':
    sp = ShopPage(open_browser())
    sp.get('http://139.129.26.163/index.php')
    sp.add_shop()
