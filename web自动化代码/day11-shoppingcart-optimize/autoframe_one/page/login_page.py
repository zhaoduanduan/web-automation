import time

from selenium.webdriver.common.by import By

from autoframe_one.common.base import Base


class LoginPage(Base):
    login_url = "http://139.129.26.163/user.php"

    def input_username(self, locator, username):
        self.send_keys(locator, username)

    def input_password(self, locator, password):
        self.send_keys(locator, password)

    def remember(self, locator):
        self.click(locator)
        time.sleep(2)

    def login(self, locator):
        self.click(locator)


if __name__ == '__main__':
    lp = LoginPage('chrome')
    lp.get('https://ecshop.test2.shopex123.com/user.php')
    lp.input_username((By.NAME, 'username'), 'zhaoduanduan')
    lp.input_password((By.NAME, 'password'), 'dd123456')
    lp.get_attribute((By.NAME, 'password'), 'outerHTML')
    lp.remember((By.ID, 'remember'))
    lp.login((By.CLASS_NAME, 'loginbtn'))
    lp.quit()
