import time
from selenium.webdriver.common.by import By
from common.base import Base
from common.functions import open_browser
from page.login_page import LoginPage
from page.shop_page import ShopPage


class CartPage(Base):
    # 删除商品
    def dele_shop(self, locator):
        self.click(locator)
        alert = self.driver.switch_to.alert
        time.sleep(2)
        alert.accept()
        time.sleep(2)

    # 加入收藏夹
    def favorites(self, locator):
        self.click(locator)
        alert = self.driver.switch_to.alert
        time.sleep(2)
        alert.accept()
        time.sleep(2)
        self.exe_script((By.XPATH, '/html/body/div[6]/div[3]/h6/span'))

    # 清空购物车
    def empty(self):
        self.click((By.XPATH, '//input[@value="清空购物车"]'))
        time.sleep(2)

    # 改变加入购物车的商品数量
    def change(self, locator, text):
        # self.click(locator)
        self.clear(locator)
        self.send_keys(locator, text)
        self.click((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[2]/td[5]'))
        time.sleep(4)

    # 更新购物车
    def update(self):
        self.click((By.XPATH, '//input[@value="更新购物车"]'))
        time.sleep(4)

    # 查看购物车
    def view(self):
        self.click((By.LINK_TEXT, '首页'))
        time.sleep(2)
        self.click((By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/a'))
        time.sleep(2)

    # 获取更改后的数值
    def get_value(self):
        self.update()
        return self.get_attribute((By.XPATH, '/html/body/div[6]/div[1]/form/table[1]/tbody/tr[2]/td[5]/input'), 'value')


if __name__ == '__main__':
    browser = open_browser()
    cp = CartPage(browser)
    lp = LoginPage(browser)
    # 登录页面网址
    lp.get('http://139.129.26.163/user.php')
    lp.input_username((By.NAME, 'username'), 'zhaoduanduan')
    lp.input_password((By.NAME, 'password'), '123456')
    lp.remember((By.ID, 'remember'))
    lp.login((By.CLASS_NAME, 'us_Submit'))
    # 首页网址
    cp.get('http://139.129.26.163/index.php')

    ShopPage(browser).add_shop()
    '''
    # 购物车页面网址
    cp.get('http://139.129.26.163/flow.php')
    # time.sleep(3)
    # 加入收藏夹
    # cp.favorites((By.XPATH,'//*[@id="formCart"]/table[1]/tbody/tr[3]/td[7]/a[2]'))
    # 删除商品
    # cp.dele_shop((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[2]/td[7]/a[1]'))
    # cp.dele_shop((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[3]/td[7]/a[1]'))
    # 清空购物车
    # cp.empty()
    # 改变数量
    cp.change((By.XPATH, '/html/body/div[6]/div[1]/form/table[1]/tbody/tr[2]/td[5]/input'),'6')
    # 更新购物车
    cp.update()
    '''
    # 查看购物车
    # cp.view()
    # 查看收藏夹商品名称
    # print(cp.favorites_name())
    cp.favorites((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[3]/td[7]/a[2]'))
    cp.change((By.XPATH, '/html/body/div[6]/div[1]/form/table[1]/tbody/tr[2]/td[5]/input'), '8')
    print(cp.get_value())
