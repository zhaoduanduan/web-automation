import time
from selenium.webdriver.common.by import By
from common.base import Base


class IndexPage(Base):

    def search_username(self):
        return self.get_element_text((By.CLASS_NAME, 'f4_b'))

    def logout_one(self):
        self.click((By.LINK_TEXT, '退出'))
        time.sleep(3)

    def logout_two(self):
        self.click((By.LINK_TEXT, '退出'))
        time.sleep(2)

    def cart_num(self):
        return self.get_element_text((By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/a'))

    # 查看收藏夹商品名称
    def favorites_name(self):
        self.click((By.XPATH, '//*[@id="ECS_MEMBERZONE"]/font/a[1]'))
        self.click((By.XPATH, '/html/body/div[6]/div[1]/div/div/div/div/a[5]'))
        self.click((By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/a'))
        return self.get_element_text((By.LINK_TEXT, '自拍杆'))
