import os.path
import time
import unittest

import HTMLTestRunner
from selenium import webdriver


def open_browser(browser='chrome'):
    if browser == 'chrome':
        return webdriver.Chrome()
    elif browser == 'firefox':
        return webdriver.Firefox()
    elif browser == 'edge':
        return webdriver.Edge()
    else:
        print('不支持该浏览器')


def report():
    report_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'report')
    # print(report_path)
    # exit(0)
    html_report_filename = time.strftime('%Y-%m-%d_%H%M%S') + '_HTMLReport.html'
    html_report_path = os.path.join(report_path, html_report_filename)
    # print(html_report_path)
    # exit(0)
    loader = unittest.defaultTestLoader.discover('case', pattern='test_shoppingcart*.py')
    with open(html_report_path, 'wb') as f:
        runner = HTMLTestRunner.HTMLTestRunner(

            title='测试报告',
            description='报告描述',
            stream=f,
            verbosity=2
        )
        runner.run(loader)

if __name__ == '__main__':
    report()
