import unittest
from test_cases import test_demo1,test_demo2

# TODO loader.loadTestsFromTestCase() 通过测试类加载
# 创建一个加载器对象
loader = unittest.TestLoader()
# 加载类
loader1 = loader.loadTestsFromTestCase(test_demo1.TestDemo1TestCase) #参数:测试类
loader2 = loader.loadTestsFromTestCase(test_demo2.TestDemo2TestCase) #参数:测试类

# 创建一个测试套件(进行组装)
suite = unittest.TestSuite()
tests = [loader1,loader2]
suite.addTests(tests)  # addTests复数形式
# 创建一个执行器,执行套件
runner = unittest.TextTestRunner()
runner.run(suite)