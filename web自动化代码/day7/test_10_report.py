import os.path
import time
import unittest
import HTMLTestRunner

# 测试报告路径
# 测试报告文件路径
report_path = os.path.abspath('report')
# print(report_path)

# 测试报告文件名
# print(time.strftime('%Y-%m-%d_%H:%M:%S'))
# html_report_filename = time.strftime('%Y-%m-%d_%H:%M:%S')+'_HTMLReport.html'
# TODO 文件名最好不要加其它的符号, 比如加了':'(冒号)会报错
html_report_filename = time.strftime('%Y-%m-%d_%H%M%S')+'_HTMLReport.html'
html_report_path = os.path.join(report_path,html_report_filename)
# print(html_report_path)

# 加载器
loader = unittest.defaultTestLoader.discover('test_cases', pattern='test*.py')

# 打开文件
with open(html_report_path, 'wb') as f: #TODO b代表二进制, 二进制模式就不用写encoding='utf8',写了会报错
    runner = HTMLTestRunner.HTMLTestRunner(

        title='测试报告',
        description='报告描述',
        stream=f,
        verbosity=2

    )
    runner.run(loader)  # TODO 这个必须在with open里面  不然会报错
