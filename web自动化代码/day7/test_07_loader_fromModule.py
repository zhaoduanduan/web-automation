from test_cases import test_demo1, test_demo2
import unittest
# loader 接收的参数是模块名
loader1 = unittest.defaultTestLoader.loadTestsFromModule(test_demo1)
loader2 = unittest.defaultTestLoader.loadTestsFromModule(test_demo2)


# suite
suite = unittest.TestSuite()
suite.addTests([loader1,loader2])


# runner
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)