import unittest

# loader
# TODO 模块名
# loader1 = unittest.defaultTestLoader.loadTestsFromName('test_cases.test_demo1')

# TODO 类名
# loader1 = unittest.defaultTestLoader.loadTestsFromName('test_cases.test_demo1.TestDemo1TestCase')

# TODO 方法名
loader1 = unittest.defaultTestLoader.loadTestsFromName('test_cases.test_demo1.TestDemo1TestCase.test1')
loader2 = unittest.defaultTestLoader.loadTestsFromName('test_cases.test_demo1.TestDemo1TestCase.test2')

# loadTestsFromName
lists = [
    'test_cases.test_demo1.TestDemo1TestCase.test1',
    'test_cases.test_demo1.TestDemo1TestCase.test2'
]
loader = unittest.defaultTestLoader.loadTestsFromNames(lists)
# suite
suite = unittest.TestSuite()
suite.addTest(loader)

# runner
runner = unittest.TextTestRunner()
runner.run(suite)
