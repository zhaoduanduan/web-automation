import unittest
import ddt
# # TODO ddt使用
# 1. 测试数据为多个字典的list类型[{},{},{}]
# 2. 测试类前加修饰@ddt.ddt
# 3. case前加修饰@ddt.data(*data_list)

@ddt.ddt
class LoginTestCase(unittest.TestCase):
    # 测试数据
    data_list = [
        {'username': 'fine1', 'password': '123456'},
        {'username': 'fine2', 'password': '123456'}
    ]

    @ddt.data(*data_list)
    def test_login(self,data):
        print(data)
        # 登录的业务
        # 1. 找到用户名框输入用户名 data['username']
        # 2. 找到密码框输入密码 data['password']
        # 3. 找到登录按钮点击


if __name__ == '__main__':
    unittest.main(verbosity=2)