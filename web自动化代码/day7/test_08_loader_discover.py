# loader
import unittest
# # TODO loader.discover(start_dir,pattern='*demo*.py') 通过目录路径加载
# start_dir:加载模块的路径
# pattern='*demo*.py': 加载模块名的规则  *通配符

loader = unittest.defaultTestLoader.discover('./test_cases', pattern='*demo*.py')

# 这里不用写suite的内容,因为只有一个loader 并且loader返回的就是suite  如果不止一个loader就要写suite组件
# TODO 单个loader就是suite
# suite


# runner
runner = unittest.TextTestRunner(verbosity=2)
runner.run(loader)