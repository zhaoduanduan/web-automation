import unittest


class MyTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print('setUpClass')

    @classmethod
    def tearDownClass(cls) -> None:
        print('tearDownClass')

    def setUp(self) -> None:
        print('setUp')

    def tearDown(self) -> None:
        print('tearDown')

    def test_01(self):
        print('test01')

    def test_02(self):
        print('test02')

    def test_03(self):
        print('test03')


if __name__ == '__main__':
    unittest.main(verbosity=2)
