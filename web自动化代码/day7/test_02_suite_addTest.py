import unittest

'''
# 创建测试套件
suite = unittest.TestSuite()
suite.addTest()
suite.addTests()
'''


class Demo1TestCase(unittest.TestCase):
    def test1(self):
        print('test1')

    def test2(self):
        print('test2')


class Demo2TestCase(unittest.TestCase):
    def testa(self):
        print('testa')

    def testb(self):
        print('testb')


if __name__ == '__main__':
    # 普通方式
    # unittest.main(verbosity=2)
    # suite 测试套件
    suite = unittest.TestSuite()
    suite.addTest(Demo1TestCase('test2'))
    suite.addTest(Demo2TestCase('testb'))
    # runner 执行器
    # 创建执行器对象 python方式执行
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)
