import time

from selenium import webdriver
from selenium.common import WebDriverException, InvalidArgumentException, NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from functions import open_browser


class Base:
    def __init__(self, browser):
        self.driver = browser
        self.driver.maximize_window()

    def get(self, url):
        try:
            self.driver.get(url)
        except (WebDriverException, InvalidArgumentException):
            print('输入的网址不正确')

    def find_ele(self, locator, timeout=5):  # locator 元祖
        try:
            return WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located(locator))
        except TimeoutException:
            print('元素定位失败')

    def click(self, locator):
        element = self.find_ele(locator)
        if element is not None:
            element.click()
        else:
            print('元素定位失败')

    # TODO  send_keys
    def send_keys(self, locator, text):
        element = self.find_ele(locator)
        if element is not None:
            element.send_keys(text)
        else:
            print('元素定位失败')

    # TODO get_attribute
    def get_attribute(self, locator, attribute):
        element = self.find_ele(locator)
        if element is not None:
            print(element.get_attribute(attribute))
        else:
            print('元素定位失败')

    # TODO get_element_text
    def get_element_text(self, locator):
        # 获取元素文本
        element = self.find_ele(locator)
        if element is not None:
            return element.text
        else:
            print('元素定位失败')

    # TODO 浏览器后退
    def back(self):
        self.driver.back()

    # TODO 浏览器前进
    def forward(self):
        self.driver.forward()

    # TODO 浏览器刷新
    def refresh(self):
        self.driver.refresh()

    def quit(self, seconds=3):
        time.sleep(seconds)
        self.driver.quit()


if __name__ == '__main__':
    brower = open_browser()
    base = Base(brower)
    base.get('https://www.baidu.com/')
    # base.find_ele((By.ID, 'kw'))
    # base.click((By.ID, 'kw'))
    # base.back()
    # time.sleep(3)
    # base.forward()
    # time.sleep(3)
    # base.refresh()
    # base.get_attribute((By.ID, 'kw'), 'outerHTML')
    print(base.get_element_text((By.LINK_TEXT, '新闻')))
    base.quit()
