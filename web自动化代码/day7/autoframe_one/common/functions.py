from selenium import webdriver


def open_browser(browser='chrome'):
    if browser == 'chrome':
        return webdriver.Chrome()
    elif browser == 'firefox':
        return webdriver.Firefox()
    elif browser == 'edge':
        return webdriver.Edge()
    else:
        print('不支持该浏览器')
