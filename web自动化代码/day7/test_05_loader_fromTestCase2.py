import unittest
from test_cases import test_demo1,test_demo2
# 创建一个加载器对象
# loader = unittest.TestLoader()
#  TODO unittest.defaultTestLoader == unittest.TestLoader() # 不写加载器对象, 默认是unittest.defaultTestLoader
# 加载类
loader1 = unittest.defaultTestLoader.loadTestsFromTestCase(test_demo1.TestDemo1TestCase)
loader2 = unittest.defaultTestLoader.loadTestsFromTestCase(test_demo2.TestDemo2TestCase)
# 创建一个测试套件(进行组装)
suite = unittest.TestSuite()
tests = [loader1,loader2]
suite.addTests(tests)
# 创建一个执行器,执行套件
runner = unittest.TextTestRunner()
runner.run(suite)