import unittest


class DemoTestCase(unittest.TestCase):
    def test01(self):
        print('test01')

    def test02(self):
        print('test02')

    def mytest(self):
        print('这个方法不会执行')


if __name__ == '__main__':
    unittest.main(verbosity=2)
