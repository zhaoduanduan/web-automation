import unittest


class SkipTestCase(unittest.TestCase):

    @unittest.skip('跳过用例的原因')
    def test01(self):
        print('test01')

    @unittest.skipIf(1 == 1, '跳过用例的原因')
    def test02(self):
        print('test02')

    @unittest.skipUnless(1 == 2, '跳过用例的原因')
    def test03(self):
        print('test03')

    @unittest.expectedFailure
    def test04(self):
        print('test04')


if __name__ == '__main__':
    unittest.main(verbosity=2)
