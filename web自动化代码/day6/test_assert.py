import unittest


class AssertTestCase(unittest.TestCase):

    def test01(self):
        self.assertEqual(1, 1, msg='不相等')

    def test02(self):
        self.assertNotEqual(1, 2, msg='相等')

    def test03(self):
        self.assertTrue(True)  # 1,2,3, ...

    def test04(self):
        self.assertFalse(False)  # 0


if __name__ == '__main__':
    unittest.main(verbosity=2)
