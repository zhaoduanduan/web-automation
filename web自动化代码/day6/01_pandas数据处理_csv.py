
from pprint import pprint
# 导入pandas
import pandas as pd

# 读取数据
data = pd.read_csv('./data/lj_data.csv', sep=',')
# print(data)
'''
# 查看前n行 head 默认查看前5行
print(data.head())
print(data.head(3))

# 查看数据的形状, 返回(行,列)
print(data.shape)

# 查看字段名 columns
print(data.columns)

# 查看索引 index
print(data.index)
'''

# # # TODO data.loc方法 根据行,列的标签值查询
# #         data.loc[行标签, 列标签]
#           data.loc[[行标签1, 行标签2, 行标签3, ...],[列标签1, 列标签2, ...]]
'''
print(data.loc[1, '区域'])
# 这里的.values会报错:'str' object has no attribute 'values' 因为只有一个值
# print(data.loc[1, '区域'].values)

print(data.loc[[1, 3, 5, 7, 9], ['区域', '户型', '面积']])
# 这里的.values不会报错, 会转变成一个列表, 因为这里获取的是多个值
print(data.loc[[1, 3, 5, 7, 9], ['区域', '户型', '面积']].values)
'''
# # TODO data.iloc方法 根据行, 列的数字位置查询, 从零开始
#         data.iloc[行索引, 列索引]
#         data.iloc[[行索引1, 行索引2, 行索引3, ...],[列索引1, 列索引2, ...]]
'''
print(data.iloc[1])
print(data.iloc[1].values) # 不报错
print(data.iloc[1, 1])
# print(data.iloc[1, 1].values) # 报错:'str' object has no attribute 'values'
                                # 因为只有一个值

print(data.iloc[[2, 4, 6, 8], [2, 4, 6]])
'''
# TODO tolist()
# 将pd读取出来的数据转换为列表[[],[],[]]
# pprint(data.iloc[[0, 1, 2, 3]].values)
# tolist()转换成pythond的列表
# pprint(data.iloc[[0, 1, 2, 3]].values.tolist())

# TODO to_dict()
# 将pd读取出来的数据转换为列表字典[{},{},{}] # 这里有疑问
'''
pprint(data.iloc[1].to_dict())
print(data.index.values) # 查看索引的值就只有数字
print(data.index) # 查看索引:RangeIndex(start=0, stop=2760, step=1)
'''
# pprint(data.iloc[[1,2,3]].to_dict())
'''
# for 循环
list1 = []
for i in data.index.values:
    list1.append(data.loc[i].to_dict())
print(len(list1))
pprint(list1)
'''
# 列表推导式
list2 = [data.loc[i].to_dict for i in data.index.values]
print(len(list2))

list2 = [data.iloc[i].to_dict for i in data.index.values]
print(len(list2))

