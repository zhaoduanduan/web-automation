import os
from pprint import pprint

import pandas as pd


class DataOperation:
    def __init__(self, path):
        self.data = pd.read_csv(path)

    def get_data_tolist(self):
        return self.data.values.tolist()

    def get_data_to_dict(self):
        return [self.data.loc[i].to_dict() for i in self.data.index.values]

if __name__ == '__main__':
    path =os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),'data/lj_data.csv')
    do = DataOperation(path)
    # pprint(do.get_data_tolist())
    pprint(do.get_data_to_dict())
