import time

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
# 打开百度
driver.get('https://www.baidu.com/')
kw = driver.find_element(By.ID, 'kw')
# 输入天气预报
kw.send_keys('天气预报')
time.sleep(2)
# 全选
kw.send_keys(Keys.CONTROL, 'a')
# 复制
kw.send_keys(Keys.CONTROL, 'c')
# 打开so
driver.get('https://www.so.com/')
ipt = driver.find_element(By.ID, 'input')
# 复制
ipt.send_keys(Keys.CONTROL, 'v')
time.sleep(2)
# 点击
driver.find_element(By.ID, 'search-button').click()
driver.quit()