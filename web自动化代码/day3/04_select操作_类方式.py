import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome()
driver.get("https://sahitest.com/demo/selectTest.htm")
s1 = driver.find_element(By.ID, 's1')
# 实例化Select类
select = Select(s1)
# 通过索引值选取option
time.sleep(3)
select.select_by_index(2)
time.sleep(3)
# 通过value值选取option
time.sleep(2)
select.select_by_value('49')
time.sleep(2)
# 通过文本选择option
time.sleep(2)
select.select_by_visible_text('Mail')
time.sleep(2)


driver.quit()