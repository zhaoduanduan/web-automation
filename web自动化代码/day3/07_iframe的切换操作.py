import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
url = 'file:///' + os.path.join(os.path.abspath('html'), 'frame.html')
driver.get(url)
# TODO 找不到, 因为在iframe标签里面 无法绕过iframe直接定位元素
# print(driver.find_element(By.ID, 'inner_h3').get_attribute('outerHTML'))
# TODO 切换到iframe1
iframe1 = driver.find_element(By.ID, 'f1')
driver.switch_to.frame(iframe1)
# 切换到iframe1就能找到了
print(driver.find_element(By.ID, 'inner_h3').get_attribute('outerHTML'))
# TODO 切换到iframe2
iframe2 = driver.find_element(By.ID, 'f2')
driver.switch_to.frame(iframe2)
print(driver.find_element(By.ID, 'p1').get_attribute('outerHTML'))
# TODO 切换到父级frame
driver.switch_to.parent_frame()
print(driver.find_element(By.ID, 'inner_h3').get_attribute('outerHTML'))
# TODO 切换到主页面
driver.switch_to.default_content()
print(driver.find_element(By.XPATH, '//h3[1]').get_attribute('outerHTML'))

time.sleep(2)
driver.quit()


