import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.find_element(By.LINK_TEXT, '新闻').click()
time.sleep(3)
print(driver.title)

print(driver.window_handles[1]) # 第二个窗口句柄
print(driver.window_handles) # 打印出的是一个列表,当前浏览器所有窗口句柄
# TODO driver.switch_to_window(窗口标识)  切换到指定窗口(标签页)
driver.switch_to.window(driver.window_handles[1]) # 切换到第二个窗口
# driver.switch_to.window(driver.current_window_handle) # 当前窗口默认第一个窗口
print(driver.title)
time.sleep(3)

driver.quit()



