import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.so.com/')
driver.find_element(By.ID, 'input').send_keys('python')
driver.find_element(By.ID, 'search-button').click()
time.sleep(3)
# 最底层
js1 = 'window.scrollTo(0,document.body.scrollHeight)'
driver.execute_script(js1) # 执行js代码
time.sleep(3)
# 最顶层
js2 = 'window.scrollTo(0,0)'
driver.execute_script(js2) # 执行js代码
time.sleep(3)
# 第二种写法  smooth(平滑滚动) instant(瞬间滚动)
time.sleep(2)
# js3 = 'window.scrollTo({left:0, top:document.body.scrollHeight, behavior:"smooth"})'
js3 = 'window.scrollTo({left:0, top:document.body.scrollHeight, behavior:"instant"})'
driver.execute_script(js3)
time.sleep(2)
# 聚焦
ele = driver.find_element(By.PARTIAL_LINK_TEXT, 'python初学者教程')
driver.execute_script('arguments[0].scrollIntoView();', ele)
time.sleep(3)


driver.quit()