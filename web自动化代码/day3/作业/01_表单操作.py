import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


class RegisteredInstance:
    def __init__(self):
        self.driver = webdriver.Chrome()

    def register(self):
        # TODO 注册操作
        url = 'file:///' + os.path.join(os.path.abspath('../html'), '注册实例.html')
        self.driver.get(url)
        self.driver.find_element(By.ID, 'user').send_keys('12345')
        time.sleep(2)
        self.driver.find_element(By.ID, 'password').send_keys('dd12345')
        time.sleep(2)
        self.driver.find_element(By.ID, 'tel').send_keys('18827020039')
        time.sleep(2)
        self.driver.find_element(By.ID, 'email').send_keys('331079445@qq.com')
        time.sleep(2)
        self.driver.find_element(By.XPATH,'//button[@value="注册"]').click()
        # TODO select下拉练习
        s1 = self.driver.find_element(By.ID, 'select')
        # 实例化Select对象
        select1 = Select(s1)
        # 通过文本选择option
        select1.select_by_visible_text('北京')
        time.sleep(2)
        select1.select_by_visible_text('上海')
        time.sleep(2)
        select1.select_by_visible_text('广州')
        time.sleep(2)
        select1.select_by_visible_text('重庆')
        time.sleep(2)
        '''
        # TODO 注册页面的alter练习
        self.driver.find_element(By.ID, 'alert').click()
        time.sleep(2)
        '''
        self.driver.find_element(By.LINK_TEXT, '退出').click()

    def register_A(self):
        # TODO 注册操作
        # url = 'file:///'+ os.path.abspath('../html') + '/注册A.html'
        url = 'file:///' + os.path.join(os.path.abspath('../html'),'注册A.html')
        self.driver.get(url)
        self.driver.find_element(By.ID, 'userA').send_keys('123456')
        time.sleep(2)
        self.driver.find_element(By.ID,'passwordA').send_keys('dd123456')
        time.sleep(2)
        self.driver.find_element(By.ID,'telA').send_keys('18827020049')
        time.sleep(2)
        self.driver.find_element(By.ID,'emailA').send_keys('331079446@qq.com')
        time.sleep(2)
        self.driver.find_element(By.XPATH,'//button[@value="注册A"]').click()
        time.sleep(2)
        # TODO select下拉练习
        s2= self.driver.find_element(By.ID, 'selectA')
        # 实例化Select类
        select2 = Select(s2)
        # 通过索引值选取option
        time.sleep(2)
        select2.select_by_index(0)
        time.sleep(2)
        select2.select_by_index(1)
        time.sleep(2)
        select2.select_by_index(2)
        time.sleep(2)
        select2.select_by_index(3)
        time.sleep(2)
        '''
        # TODO 注册页面的alter练习
        self.driver.find_element(By.ID, 'alerta').click()
        time.sleep(2)
        '''
        self.driver.find_element(By.LINK_TEXT, '退出').click()

    def register_B(self):
        # TODO 注册操作
        url = 'file:///' + os.path.join(os.path.abspath('../html'),'注册B.html')
        self.driver.get(url)
        self.driver.find_element(By.ID, 'userB').send_keys('1234567')
        time.sleep(2)
        self.driver.find_element(By.ID, 'passwordB').send_keys('dd123456')
        time.sleep(2)
        self.driver.find_element(By.ID, 'telB').send_keys('18827020059')
        time.sleep(2)
        self.driver.find_element(By.ID, 'emailB').send_keys('331079447@qq.com')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//button[@value="注册B"]').click()
        time.sleep(2)
        # TODO select下拉练习
        s3 = self.driver.find_element(By.ID, 'selectB')
        # 实例化select对象
        select3 = Select(s3)
        # 通过value值选取option
        select3.select_by_value('bj')
        time.sleep(2)
        select3.select_by_value('sh')
        time.sleep(2)
        select3.select_by_value('gz')
        time.sleep(2)
        select3.select_by_value('cq')
        '''
        # TODO 注册页面的alter练习
        self.driver.find_element(By.ID, 'alertB').click()
        time.sleep(2)
        '''
        self.driver.find_element(By.LINK_TEXT, 'B退出').click()

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    re = RegisteredInstance()
    re.register()
    re.register_A()
    re.register_B()
    re.quit()
