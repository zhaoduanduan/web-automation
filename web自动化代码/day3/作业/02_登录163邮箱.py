import time

from selenium import webdriver
from selenium.webdriver.common.by import By


class Login:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://mail.163.com/')

    def login_mail(self):
        # 账号输入
        # iframe1 = self.driver.find_element(By.XPATH, '//iframe[1]')
        # 模糊定位
        # iframe1 = self.driver.find_element(By.XPATH, '//iframe[contains(@id, "x-URS-iframe")]') #这个也可以
        iframe1 = self.driver.find_element(By.XPATH, '//*[contains(@id, "x-URS-iframe")]') #这个也可以
        self.driver.switch_to.frame(iframe1)
        self.driver.find_element(By.XPATH, '//input[1]').send_keys('zhaoduanduan_lucky')
        # 密码输入
        self.driver.find_element(By.NAME, 'password').send_keys('Dd123456')
        # 点击登录
        self.driver.find_element(By.ID, 'dologin').click()

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    lo = Login()
    lo.login_mail()
    time.sleep(3)
    lo.quit()
