import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get("https://sahitest.com/demo/selectTest.htm")
time.sleep(3)
select = driver.find_element(By.ID, 's1')
option = select.find_element(By.XPATH, '//select[@id="s1"]/option[2]')
print(option.get_attribute('outerHTML'))
option.click()

time.sleep(3)
driver.quit()