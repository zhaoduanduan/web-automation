import os.path
import time

from selenium import webdriver

from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


class MouseOperation:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://sahitest.com/demo/clicks.htm")
        self.driver.maximize_window()

    # 单击
    def click(self):
        # 第一种方式
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//form/input[3]').click()
        # 第二种方式
        time.sleep(3)
        btn = self.driver.find_element(By.XPATH, '//form/input[3]')
        ac = ActionChains(self.driver)
        ac.click(btn).perform()

    # 右击
    def context_click(self):

        time.sleep(3)
        btn = self.driver.find_element(By.XPATH, "//form/input[4]")
        ac = ActionChains(self.driver)
        ac.context_click(btn).perform()
        time.sleep(2)

    # 双击
    def double_click(self):
        time.sleep(3)
        btn = self.driver.find_element(By.XPATH, '//form/input[2]')
        ac = ActionChains(self.driver)
        ac.double_click(btn).perform()
        time.sleep(2)

    # 拖动
    def drag_and_drop(self):
        self.driver.get('https://sahitest.com/demo/dragDropMooTools.htm')
        time.sleep(3)
        source = self.driver.find_element(By.ID, 'dragger')
        target = self.driver.find_element(By.XPATH, '//div[4]')
        ac = ActionChains(self.driver)
        ac.drag_and_drop(source, target).perform()
        time.sleep(3)

    # 悬停
    def move_to_element(self):
        self.driver.get('https://sahitest.com/demo/mouseover.htm')
        time.sleep(3)
        ele = self.driver.find_element(By.XPATH, '//div/div')
        ac = ActionChains(self.driver)
        ac.move_to_element(ele).perform()
        time.sleep(3)

    # 长按
    def click_and_hold(self):
        # 请求本地页面
        # url = 'file:///' + os.path.abspath('html') + '/mouse_hold.html'
        url = 'file:///' + os.path.join(os.path.abspath('html'), 'mouse_hold.html')
        self.driver.get(url)
        time.sleep(2)
        btn = self.driver.find_element(By.ID, 'btn1')
        ac = ActionChains(self.driver)
        ac.click_and_hold(btn).perform()
        time.sleep(5)


    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    mo = MouseOperation()
    mo.click()
    mo.context_click()
    mo.double_click()
    mo.drag_and_drop()
    mo.move_to_element()
    mo.click_and_hold()
    mo.quit()
