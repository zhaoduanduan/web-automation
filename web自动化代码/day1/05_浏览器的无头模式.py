from selenium import webdriver

# 选项对象, 添加参数
options = webdriver.ChromeOptions()
options.add_argument('-headless')

# 打开浏览器, 设置选项
driver = webdriver.Chrome(options=options)

# 请求360搜索
driver.get('https://www.so.com/')

# 输出网页标题
print(driver.title)

# 退出浏览器
driver.quit()

