import time

from selenium import webdriver
from selenium.webdriver.common.by import By


class BrowserOperation:
    def __init__(self,url):
        self.driver = webdriver.Chrome()
        self.driver.get(url)

    def search_weather(self, seconds=3):
        self.driver.find_element(By.ID, 'query').send_keys('天气预报')
        self.driver.find_element(By.ID, 'stb').click()
        time.sleep(seconds)
        print(self.driver.title)

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    # BrowserOperation('https://www.sogou.com/')
    # BrowserOperation('https://www.sogou.com/').search_weather()
    # BrowserOperation('https://www.sougou.com/').quit()

    oper = BrowserOperation('https://www.sogou.com/')
    # oper.search_weather()
    # oper.quit()

