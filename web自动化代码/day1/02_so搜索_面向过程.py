# 0 导包
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

# 1.打开Chrome, 打开so首页
driver = webdriver.Chrome()
driver.get("https://www.so.com/")
# 2.搜索框输入"python自动化测试",点击搜索
# driver.find_element_by_id('kw') # selenium4之前的方式, 淘汰掉
# driver.find_element('id', 'kw') # 不推荐
kw = driver.find_element(By.ID, "input") # 推荐
# send_keys() 文本框写入值
kw.send_keys("python自动化测试")
# 点击搜索按钮
su = driver.find_element(By.ID, "search-button")
# 按钮点击 click()
su.click()

# 3.获取页面标题, 并打印出来
# 页面标题 driver.title
time.sleep(3) # 页面停留3秒
print(driver.title)
# 4.检查'python'关键字是否在标题中
if "python" in driver.title:
    print("找到了")
else:
    print("没找到")
# 5.关闭Chrome浏览器
time.sleep(3)
driver.quit()