from selenium import webdriver
from selenium.webdriver.common.by import By
import time

class TestCase:
    def __init__(self):
        self.driver = webdriver.Chrome()

    def get_so(self):
        # 请求so页面
        self.driver.get('https://www.so.com/')
        # 定位搜索框, 写入文本信息
        self.driver.find_element(By.ID, 'input').send_keys('python自动化测试')
        # 定位到搜索按钮, 点击
        self.driver.find_element(By.ID, 'search-button').click()
        # 获取页面标题
        time.sleep(3)
        print(self.driver.title)
        # 判断python是否在页面标题中
        if 'python' in self.driver.title:
            print('找到了')
        else:
            print('没找到')


    # def __del__(self):
    #     self.driver.quit() # 这个方法不行

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    case = TestCase()
    case.get_so()
    case.quit()

