# 导包
import time

from selenium import webdriver

# 打开浏览器
driver = webdriver.Chrome()

# 退出浏览器
time.sleep(3) # 使浏览器页面停留3秒
driver.quit() # 退出浏览器