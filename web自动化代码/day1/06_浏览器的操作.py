import time

from selenium import webdriver
from selenium.webdriver.common.by import By


class BrowserOperation:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com')

    def quit(self,seconds=2):
        time.sleep(seconds)
        self.driver.quit()

    # 浏览器窗口大小调整
    def window_size(self):
        self.driver.set_window_size(480,800)
        time.sleep(3)
        # 最大化
        self.driver.maximize_window()

     # 页面的前进后退和刷新
    def forward_back(self):
        # 后退
        self.driver.back()
        time.sleep(3)
        # 前进
        self.driver.forward()
        # 刷新
        self.driver.refresh()

    # 浏览器打开新闻和关闭窗口
    def close(self):
        self.driver.find_element(By.LINK_TEXT, '新闻').click()
        time.sleep(2)
        self.driver.close() # 默认关闭的是第一个窗口

if __name__ == '__main__':
    oper = BrowserOperation()
    oper.window_size()
    oper.forward_back()
    # oper.close()
    oper.quit()
