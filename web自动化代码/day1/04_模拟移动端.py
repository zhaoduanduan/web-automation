import time

from selenium import webdriver
# 准备配置项
mobileEmulation = {'deviceName': 'iPhone SE'}
# 创建options对象
options = webdriver.ChromeOptions()
# 添加选项配置信息
options.add_experimental_option('mobileEmulation',mobileEmulation)
# 打开浏览器, 配置选项对象
driver = webdriver.Chrome(options=options)
# 访问网址
driver.get('https://www.so.com/')
# 退出浏览器
time.sleep(3)
driver.quit()
