from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()

# 基础选择器
# id选择器
print(driver.find_element(By.CSS_SELECTOR, '#kw').get_attribute('outerHTML'))
# class选择器
print(driver.find_element(By.CSS_SELECTOR, '.s_ipt').get_attribute('outerHTML'))
# 标签 + 属性
print(driver.find_element(By.CSS_SELECTOR, 'input[name = "wd"]').get_attribute('outerHTML'))

# 层级
print(driver.find_element(By.CSS_SELECTOR, "form[id='form'] input").get_attribute("outerHTML"))
# 课后练习 find_elements 找子元素 遍历列表,查看每个子元素的outerHTML
# 索引
#
print("*"*100)
eles = driver.find_elements(By.CSS_SELECTOR, "form[id='form'] input")
for ele in eles:
    print(ele.get_attribute("outerHTML"))
print("*"*100)
print(driver.find_element(By.CSS_SELECTOR, "form[id='form'] :nth-child(8)").get_attribute("outerHTML"))
print(driver.find_element(By.CSS_SELECTOR, "form[id='form'] input:nth-of-type(8)").get_attribute("outerHTML"))

# 模糊匹配关键字
print(driver.find_element(By.CSS_SELECTOR, "input[name*='_idx']").get_attribute("outerHTML"))

# 多条件查找
print(driver.find_element(By.CSS_SELECTOR, "input[name='wd'][autocomplete='off']").get_attribute("outerHTML"))

driver.quit()