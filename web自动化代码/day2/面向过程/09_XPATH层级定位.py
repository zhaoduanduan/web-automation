from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
# 打印出form下面的第一个span标签下面的input
print(driver.find_element(By.XPATH, '//form/span/input').get_attribute('outerHTML'))
# 打印出form下面的第一个span标签下面所有内容
print(driver.find_element(By.XPATH, '//form/span').get_attribute('outerHTML'))


driver.quit()