from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
# 元素是否可见
ie = driver.find_element(By.XPATH, '//form[@id="form"]/input[1]')
print(ie.get_attribute('outerHTML'))
print(ie.is_displayed())
kw = driver.find_element(By.ID, 'kw')
print(kw.is_displayed())

driver.quit()
