from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://sahitest.com/demo/clicks.htm')

# 元素是否可用
button = driver.find_element(By.XPATH, '//form[@name="f1"]/input[@value="disable1"]')
print(button.get_attribute('outerHTML'))
print(button.is_enabled())

button2 = driver.find_element(By.XPATH,'//form[@name="f1"]/input[2]')
print(button2.get_attribute('outerHTML'))
print(button2.is_enabled())

driver.quit()