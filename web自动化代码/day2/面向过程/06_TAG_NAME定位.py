from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
# 默认查找第一个标签
input = driver.find_element(By.TAG_NAME, 'input')
print(input)
print(input.get_attribute('outerHTML'))
print('*'*100)
# 查找所有标签
inputs = driver.find_elements(By.TAG_NAME, 'input')
print(inputs)
print(len(inputs))
print('*'*150)
print(inputs[2].get_attribute('outerHTML'))
for ele in inputs:
    print(ele.get_attribute('outerHTML'))

#  print(inputs.get_attribute('outerHTML'))  列表不行, 只有单个元素才有get_attribute属性
driver.quit()




