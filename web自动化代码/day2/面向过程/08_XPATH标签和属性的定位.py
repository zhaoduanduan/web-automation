from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()
# 根据标签和属性定位搜索框
kw = driver.find_element(By.XPATH, '//input[@id="kw"]')
print(kw.get_attribute('outerHTML'))

maxlength = driver.find_element(By.XPATH, '//input[@maxlength="255"]')
print(maxlength.get_attribute('outerHTML'))

s_ipt = driver.find_element(By.XPATH, '//input[@class="s_ipt"]')
print(s_ipt.get_attribute('outerHTML'))

# 关闭浏览器
driver.quit()