from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()
print(driver.find_element(By.XPATH, '//a[contains(text(), "hao")]').get_attribute('outerHTML'))
print(driver.find_element(By.XPATH, '//input[contains(@id, "k")]').get_attribute('outerHTML')) # 模糊查询也可以适用于其它属性


driver.quit()