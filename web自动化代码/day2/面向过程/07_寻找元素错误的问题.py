from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
# kw = driver.find_element(By.CLASS_NAME, 'aaaa') # CLASS_NAME的值没有aaaa,所以会报错
kw = driver.find_elements(By.CLASS_NAME, 'aaaa') # CLASS_NAME的值没有aaaa,因为是find_elements复数形式,
                                                 # 所以不会报错,会返回一个空列表

print(kw)


# print(kw.get_attribute('outerHTML'))


driver.quit()