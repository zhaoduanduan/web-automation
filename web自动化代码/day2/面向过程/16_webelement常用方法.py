# send_keys() clear() click() get_attribute()
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()

kw = driver.find_element(By.ID, 'kw')
kw.send_keys('python自动化测试')
time.sleep(3)
kw.clear() # 清除文本内容
kw.send_keys('天气预报')
print(kw.get_attribute('id'))
driver.find_element(By.ID, 'su').click()

time.sleep(3)
driver.quit()