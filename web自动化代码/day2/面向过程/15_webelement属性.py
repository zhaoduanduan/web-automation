from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()
kw = driver.find_element(By.ID, 'kw')

# 元素ID
print(kw.id)

# size 宽高
print(kw.size)

# rect 宽高和坐标
print(kw.rect)

# tag_name 标签名
print(kw.tag_name)

# text 文本内容
print(kw.text)


driver.quit()