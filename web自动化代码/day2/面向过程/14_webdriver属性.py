from selenium import webdriver

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()

# 浏览器名称
print(driver.name)

# 当前url
print(driver.current_url)

# 当前页面的标题
print(driver.title)

# 当前页面的代码
# print(driver.page_source)

# 当前窗口句柄
print(driver.current_window_handle)

# 当前浏览器所有句柄
print(driver.window_handles)
driver.quit()
