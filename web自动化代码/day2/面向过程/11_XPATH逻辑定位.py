from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()
print(driver.find_element(By.XPATH, '//input[@id="kw" and @class = "s_ipt"]').get_attribute('outerHTML'))

driver.quit()