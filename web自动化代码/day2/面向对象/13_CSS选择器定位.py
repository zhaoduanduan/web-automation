from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def css(self):
        # id选择器, #就是id
        print(self.driver.find_element(By.CSS_SELECTOR, '#kw').get_attribute('outerHTML'))
        # class选择器, .就是class
        print(self.driver.find_element(By.CSS_SELECTOR, '.s_ipt').get_attribute('outerHTML'))
        # 标签 + 属性
        print(self.driver.find_element(By.CSS_SELECTOR, 'input[name="wd"]').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    position = Positioning()
    position.css()
    position.quit()
