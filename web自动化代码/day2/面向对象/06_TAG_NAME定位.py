from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')

    def tag_name(self):
        # 默认查找第一个标签
        print(self.driver.find_element(By.TAG_NAME, 'input').get_attribute('outerHTML'))
        print('*'*150)
        # 查找所有标签
        inputs = self.driver.find_elements(By.TAG_NAME, 'input')  # find_elements返回的是一个列表
        print(len(inputs))
        print(inputs[2].get_attribute('outerHTML')) # 列表里的第三个
        for ele in inputs:
            print(ele.get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    position = Positioning()
    position.tag_name()
    position.quit()


