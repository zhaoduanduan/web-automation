from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')

# kw = driver.find_element(By.CLASS_NAME, 'aaa') # CLASS_NAME的值没有aaa, 所以会报错
print(driver.find_elements(By.CLASS_NAME, 'aaa')) # CLASS_NAME的值没有aaa,因为是find_elements复数形式,
                                                  # 所以不会报错,但是会返回一个空列表


driver.quit()