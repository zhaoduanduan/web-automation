from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')

    def xpath_index(self):
        print(self.driver.find_element(By.XPATH, '//form/input[2]').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    position = Positioning()
    position.xpath_index()
    position.quit()
