from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def xpath_logic(self):
        print(self.driver.find_element(By.XPATH, '//input[@id="kw" and @class="s_ipt"]').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    position = Positioning()
    position.xpath_logic()
    position.quit()

