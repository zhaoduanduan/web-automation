from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')

    def partial_link_text(self):
        print(self.driver.find_element(By.PARTIAL_LINK_TEXT, 'hao').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    position = Positioning()
    position.partial_link_text()
    position.quit()