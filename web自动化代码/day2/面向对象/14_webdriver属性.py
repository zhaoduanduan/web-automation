from selenium import webdriver


class WebdriverAttribute:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def attribute(self):
        # 浏览器名称
        print(self.driver.name)
        # 当前url
        print(self.driver.current_url)
        # 当前页面的标题
        print(self.driver.title)
        # 当前页面的代码
        # print(self.driver.page_source)
        # 当前窗口句柄
        print(self.driver.current_window_handle)
        # 当前浏览器所有句柄
        print(self.driver.window_handles)

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    web = WebdriverAttribute()
    web.attribute()
    web.quit()

