from selenium import webdriver
from selenium.webdriver.common.by import By


class TestCase:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://sahitest.com/demo/clicks.htm')
        self.driver.maximize_window()

    def is_enabled(self):
        print(self.driver.find_element(By.XPATH, '//form[@name="f1"]/input[@value="disable1"]').get_attribute('outerHTML'))
        print(self.driver.find_element(By.XPATH, '//form[@name="f1"]/input[@value="disable1"]').is_enabled())

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    case = TestCase()
    case.is_enabled()
    case.quit()