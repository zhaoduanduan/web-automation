from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def xpath_fuzzy(self):
        print(self.driver.find_element(By.XPATH, '//a[contains(text(), "hao")]').get_attribute('outerHTML'))
        # 模糊查询也可以适用于其它属性
        print(self.driver.find_element(By.XPATH, '//input[contains(@id, "k")]').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    position = Positioning()
    position.xpath_fuzzy()
    position.quit()