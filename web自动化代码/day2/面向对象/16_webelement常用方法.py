# send_keys() clear() click() get_attribute()
import time

from selenium import webdriver
from selenium.webdriver.common.by import By


class TestCase:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def case(self):
        self.driver.find_element(By.ID, 'kw').send_keys('python自动化测试')
        time.sleep(3)
        self.driver.find_element(By.ID, 'kw').clear()  # 清除文本内容
        self.driver.find_element(By.ID, 'kw').send_keys('天气预报')
        print(self.driver.find_element(By.ID, 'kw').get_attribute('id'))
        self.driver.find_element(By.ID, 'su').click()
        time.sleep(3)

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    testcase = TestCase()
    testcase.case()
    testcase.quit()
