from selenium import webdriver
from selenium.webdriver.common.by import By


class WebelementAttribute:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def attribute(self):
        # 元素id
        print(self.driver.find_element(By.ID, 'kw').id)
        # size 宽高
        print(self.driver.find_element(By.ID, 'kw').size)
        # rect 宽高和坐标
        print(self.driver.find_element(By.ID, 'kw').rect)
        # tag_name 标签名
        print(self.driver.find_element(By.ID, 'kw').tag_name)
        # text 文本内容
        print(self.driver.find_element(By.LINK_TEXT, '新闻').text)

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    webele = WebelementAttribute()
    webele.attribute()
    webele.quit()