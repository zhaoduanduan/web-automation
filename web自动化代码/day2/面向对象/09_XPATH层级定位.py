from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')

    def xpath_hierarchy(self):
        # 打印出form下面的第一个span标签下面的第一个input
        print(self.driver.find_element(By.XPATH, '//form/span/input').get_attribute('outerHTML'))
        # 打印出form下面的第一个span标签下面的所有内容
        print(self.driver.find_element(By.XPATH, '//form/span').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    position = Positioning()
    position.xpath_hierarchy()
    position.quit()