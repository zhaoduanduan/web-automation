from selenium import webdriver
from selenium.webdriver.common.by import By


class Positioning:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://www.baidu.com/')
        self.driver.maximize_window()

    def xpath(self):
        print(self.driver.find_element(By.XPATH, '//input[@id="kw"]').get_attribute('outerHTML'))
        print(self.driver.find_element(By.XPATH, '//input[@maxlength="255"]').get_attribute('outerHTML'))
        print(self.driver.find_element(By.XPATH, '//input[@class="s_ipt"]').get_attribute('outerHTML'))

    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    position = Positioning()
    position.xpath()
    position.quit()