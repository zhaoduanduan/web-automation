import time
from selenium import webdriver
from selenium.webdriver.common.by import By


class BrowserOperation:
    # 初始化对象属性
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://ecshop.test2.shopex123.com/')
        self.driver.maximize_window()
        # time.sleep(3)

    # 首页搜索操作
    def homepage_search(self, seconds=3):
        self.driver.find_element(By.ID, 'keyword').send_keys('悦诗风吟')
        self.driver.find_element(By.CLASS_NAME, 'btn_search').click()
        print(self.driver.find_element(By.CLASS_NAME, 'btn_search').get_attribute('outerHTML'))
        time.sleep(seconds)

    # 注册操作
    def registered(self):
        self.driver.find_element(By.LINK_TEXT, '[ 免费注册 ]').click()
        self.driver.find_element(By.ID, 'username').send_keys('zhaoduanduan')
        self.driver.find_element(By.ID, 'email').send_keys('331079447@qq.com')
        self.driver.find_element(By.ID, 'password1').send_keys('dd123456')
        self.driver.find_element(By.ID, 'confirm_password').send_keys('dd123456')
        self.driver.find_element(By.CSS_SELECTOR, 'input[name = "Submit"]').click()
        # print(self.driver.find_element(By.CSS_SELECTOR, 'input[name = "Submit"]').get_attribute('outerHTML'))

    # 登录操作
    def login(self):
        self.driver.find_element(By.LINK_TEXT, '[ 请登录 ]').click()
        self.driver.find_element(By.NAME, 'username').send_keys('zhaoduanduan')
        self.driver.find_element(By.NAME, 'password').send_keys('dd123456')
        self.driver.find_element(By.NAME, 'submit').click()

    # 关闭浏览器
    def quit(self):
        self.driver.quit()


if __name__ == '__main__':
    oper = BrowserOperation()
    # oper.homepage_search()
    # oper.registered()
    oper.login()
    # oper.quit()
