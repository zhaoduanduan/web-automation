import time

from selenium import webdriver
# TODO 导入显示等待
from selenium.webdriver.support.wait import WebDriverWait

# 显示等待针对的是某一个标签的状态
# 检测百度首页的title是不是'xxxx'
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
time.sleep(2)
# TODO lambda函数
func = lambda x: True if x.title =='百度一下，你就知道' else False
print(func(driver))

# TODO 等待True,超时没有返回True,就抛出异常  until
result1 = WebDriverWait(driver, 10).until(lambda x:True if x.title =='百度一下，你就知道' else False, message='标题不一致')
print(result1)
# selenium.common.exceptions.TimeoutException: Message: 标题不一致 超时没有判定成功抛出异常

# TODO 等待False, 超时没有返回False, 就抛出异常
result2 = WebDriverWait(driver, 10).until_not(lambda x: True if x.title == '百度一下，你就知道1' else False, message='标题一致')
# 老师写的是标message='标题不一致',我觉得是message='标题一致'
print(result2)


# print(driver.title)

# func = lambda x: True if x.title =='百度一下，你就知道' else False
# print(func(driver))

# # until
# result1 = WebDriverWait(driver,10,poll_frequency=1).until(lambda x: True if x.title =='百度一下，你就知道' else False,message='超时')
# print(result1)
#
# # until_not
# result2 = WebDriverWait(driver,10,poll_frequency=1).until_not(lambda x: True if x.title =='百度一下，你就知道' else False,message='超时')
# print(result2)

driver.quit()