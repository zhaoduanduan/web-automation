from selenium import webdriver
from selenium.webdriver.common.by import By
# TODO 导入显示等待
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
# 检测百度首页的title是不是'xxxx'
'''
# TODO EC.title_is(str) 判断页面title是否与传入值str完全一致(大小写敏感)
# TODO 等待True, 超时没有返回True, 就抛出异常
result1 = WebDriverWait(driver,10).until(EC.title_is('百度一下，你就知道'), message='标题不一致')
print(result1)

# TODO 等待False, 超时没有返回False,就抛出异常
result2 = WebDriverWait(driver, 10).until_not(EC.title_is('百度一下，你就知道1'), message='标题一致')
print(result2)
'''
# TODO EC.presence_of_element_located(locator)
# 判断一个元素存在于DOM树中,存在则返回元素本身, 不存在则报错.locator为一个(By,value)元祖
kw = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'kw')))
print(kw)
# TODO *拆包
t1 = (By.ID, 'kw')
l1 = [By.ID, 'kw']
kw1 = driver.find_element(*t1)
kw2 = driver.find_element(*l1)
print(kw1.get_attribute('outerHTML'))
print(kw2.get_attribute('outerHTML'))



# kw = WebDriverWait(driver,10).until(EC.presence_of_element_located((By.ID, 'kw')))
