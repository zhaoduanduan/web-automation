import time

from selenium import webdriver
from selenium.common import WebDriverException, InvalidArgumentException, NoSuchElementException
from selenium.webdriver.common.by import By


class Base:
    def __init__(self, browser):
        if browser == 'chrome':
            self.driver = webdriver.Chrome()
        elif browser == 'firefox':
            self.driver = webdriver.Firefox()
        elif browser == 'edge':
            self.driver = webdriver.Edge()
        else:
            print('不支持该浏览器')
            self.driver = None
        self.driver.maximize_window()

    def get(self, url):
        try:
            self.driver.get(url)
        except (WebDriverException, InvalidArgumentException):
            print('输入的网址不正确')

    def find_ele(self, locator):
        try:
            return self.driver.find_element(*locator)
        except NoSuchElementException:
            print('元素定位失败')

    def click(self, locator):
        element = self.find_ele(locator)
        if element is not None:
            element.click()
        else:
            print('元素定位失败')

    # TODO  send_keys
    def send_keys(self, locator, text):
        element = self.find_ele(locator)
        if element is not None:
            element.send_keys(text)
        else:
            print('元素定位失败')

    # TODO get_attribute
    def get_attribute(self, locator,attribute):
        element = self.find_ele(locator)
        if element is not None:
            print(element.get_attribute(attribute))
        else:
            print('元素定位失败')

    # TODO 浏览器后退
    def back(self):
        self.driver.back()

    # TODO 浏览器前进
    def forward(self):
        self.driver.forward()

    # TODO 浏览器刷新
    def refresh(self):
        self.driver.refresh()

    def quit(self, seconds=3):
        time.sleep(seconds)
        self.driver.quit()


if __name__ == '__main__':
    base = Base('chrome')
    base.get('https://www.baidu.com/')
    base.find_ele((By.ID, 'kw'))
    base.click((By.ID, 'kw'))
    base.back()
    time.sleep(3)
    base.forward()
    time.sleep(3)
    base.refresh()
    base.get_attribute((By.ID, 'kw'),'outerHTML')
    base.quit()
