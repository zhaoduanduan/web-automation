from selenium.webdriver.common.by import By

from web自动化代码.day5.autoframe1.common.base import Base


class LoginPage(Base):
    def input_username(self, username):
        self.find_element((By.NAME, 'username')).send_keys(username)

    def input_password(self,password):
        self.find_element((By.NAME, 'password')).send_keys(password)

    def login(self):
        self.click((By.CLASS_NAME, 'loginbtn'))

if __name__ == '__main__':
    login = LoginPage('chrome')
    login.get('https://ecshop.test2.shopex123.com/user.php')
    login.input_username('zhaoduanduan')
    login.input_password('dd123456')
    login.login()
