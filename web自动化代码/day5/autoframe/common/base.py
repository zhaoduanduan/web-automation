import time

from selenium import webdriver
from selenium.common import WebDriverException, NoSuchElementException, InvalidArgumentException
from selenium.webdriver.common.by import By


class Base:
    def __init__(self, browser):
        if browser == 'chrome':
            self.driver = webdriver.Chrome()
        elif browser == 'firefox':
            self.driver = webdriver.Firefox()
        elif browser == 'edge':
            self.driver = webdriver.Edge()
        else:
            print('不支持该浏览器')
            self.driver = None

    def get(self, url):
        self.driver.get(url)
        '''
        try:
             self.driver.get(url)
        except (WebDriverException, InvalidArgumentException):
            print('输入的网址不正确')
        return self.driver.get(url)
        '''

    def find_element(self, locator):
        try:
            # ele = self.driver.find_element(*locator)
            # print(ele.get_attribute('outerHTML'))
            # return ele
            return self.driver.find_element(*locator)
        except NoSuchElementException:
            print('元素定位失败')
        # return self.driver.find_element(*locator)

    def click(self, locator):
        element = self.find_element(locator)
        if element is not None:
            element.click()
        else:
            print('元素定位失败')


def quit(self, seconds=3):
    time.sleep(seconds)
    self.driver.quit()


if __name__ == '__main__':
    base = Base('chrome')
    base.get('https://www.baidu.com/')
    # print(base.find_element((By.ID, 'kw')).get_attribute('outerHTML'))
    print(base.find_element((By.ID, 'kw')))
