import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')

# TODO 设置隐式等待
# TODO 在请求网址后, 定位元素前, 为整个driver设置隐式等待.保证在定位元素前, 元素已经全部加载
driver.implicitly_wait(10)

kw = driver.find_element(By.ID, 'kw')
print(kw.get_attribute('outerHTML'))

time.sleep(3)
driver.quit()