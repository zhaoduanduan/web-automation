import time

from selenium.webdriver.common.by import By

from common.base import Base
from common.functions import open_browser
from page.login_page import LoginPage


class IndexPage(Base):
    # @classmethod
    # def aaa(cls):
    #     browser = open_browser()
    #     cls.login = LoginPage(browser)

    def search_username(self):
        return self.get_element_text((By.CLASS_NAME, 'f4_b'))

    def logout_one(self):
        self.click((By.LINK_TEXT, '退出'))
        time.sleep(3)

    def logout_two(self):
        self.click((By.LINK_TEXT, '退出'))
        time.sleep(2)

    def cart_num(self):
        return self.get_element_text((By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/a'))

    # 查看收藏夹商品名称
    def favorites_name(self):
        self.click((By.XPATH, '//*[@id="ECS_MEMBERZONE"]/font/a[1]'))
        self.click((By.XPATH, '/html/body/div[6]/div[1]/div/div/div/div/a[5]'))
        self.click((By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/a'))
        return self.get_element_text((By.LINK_TEXT, '自拍杆'))




    # def favorites_name(self):
    #     # self.get('http://139.129.26.163/user.php')
    #     # self.login.input_username((By.NAME, 'username'), 'zhaoduanduan')
    #     # self.login.input_password((By.NAME, 'password'), '123456')
    #     # self.login.remember((By.ID, 'remember'))
    #     # self.login.login((By.CLASS_NAME, 'us_Submit'))
    #     self.click((By.LINK_TEXT, '用户中心'))
    #     self.click((By.LINK_TEXT, ' 我的收藏'))


if __name__ == '__main__':
    browser = open_browser()
    ip = IndexPage(browser)
    # login = LoginPage(browser)
    # login.get('http://139.129.26.163/user.php')
    # login.input_username((By.NAME, 'username'), 'zhaoduanduan')
    # login.input_password((By.NAME, 'password'), '123456')
    # login.remember((By.ID, 'remember'))
    # login.login((By.CLASS_NAME, 'us_Submit'))
    # ip.aaa()
    # browser = open_browser()
    # login = LoginPage(browser)
    # ip.favorites_name()
