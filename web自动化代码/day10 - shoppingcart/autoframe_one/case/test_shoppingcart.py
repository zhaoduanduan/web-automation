import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from page.cart_page import CartPage
from page.index_page import IndexPage
from page.shop_page import ShopPage


class ShoppingTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # 打开浏览器
        # browser = open_browser()

        # TODO 免登录 配置加载项
        options = webdriver.ChromeOptions()
        user_data_dir = r'--user-data-dir=C:\Users\戴先生\AppData\Local\Google\Chrome\User Data'
        options.add_argument(user_data_dir)
        driver = webdriver.Chrome(options=options)
        driver.get('http://139.129.26.163/index.php')
        cls.shoppage = ShopPage(driver)
        cls.cartpage = CartPage(driver)
        cls.indexpage = IndexPage(driver)

        # time.sleep(3)
        # cls.shoppage.click((By.LINK_TEXT, '首页'))

    @classmethod
    def tearDownClass(cls) -> None:
        # 退出登录
        # cls.indexpage.logout_two()
        # 退出浏览器
        cls.shoppage.quit()

    # TODO 添加商品到购物车
    def test01_shop_page(self):
        self.shoppage.add_shop()
        # 断言
        cart_num = self.indexpage.cart_num()
        self.assertEqual(cart_num, '购物车(4)', msg='加入失败')

    # TODO  购物车页面的操作
    def test02_cart_page(self):
        # TODO 把商品加入收藏夹
        self.cartpage.favorites((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[3]/td[7]/a[2]'))
        # 断言
        name = self.indexpage.favorites_name()
        self.assertEqual(name, '自拍杆', msg='收藏失败')
        # TODO 改变加入购物车商品的数量
        self.cartpage.change((By.XPATH, '/html/body/div[6]/div[1]/form/table[1]/tbody/tr[2]/td[5]/input'), '8')
        # 断言
        value = self.cartpage.get_value()
        self.assertEqual(value, '8', msg='更改失败')
        # TODO 删除商品
        self.cartpage.dele_shop((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[2]/td[7]/a[1]'))
        self.cartpage.dele_shop((By.XPATH, '//*[@id="formCart"]/table[1]/tbody/tr[3]/td[7]/a[1]'))
        # 断言
        cart_num_one = self.indexpage.cart_num()
        self.assertEqual(cart_num_one, '购物车(1)', msg='删除失败')
        # TODO 更新购物车
        self.cartpage.update()
        # TODO 回到首页查看购物车
        self.cartpage.view()
        # TODO 清空购物车
        self.cartpage.empty()
        # 断言
        cart_num_two = self.indexpage.cart_num()
        self.assertEqual(cart_num_two, '购物车(0)', msg='清空失败')


if __name__ == '__main__':
    unittest.main(verbosity=2)
