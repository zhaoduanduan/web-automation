import os.path
import time
import unittest

import ddt
from selenium.webdriver.common.by import By
from common.functions import open_browser
from common.base import Base
from common.data_operation import DataOperation

from page.index_page import IndexPage
from page.login_page import LoginPage

project_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path = os.path.join(project_path, 'data/login.csv')
account_list = DataOperation(path).get_data_to_dict()


# print(account_list)


@ddt.ddt
class LoginTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # 打开浏览器
        browser = open_browser()
        # 创建page页面对象
        cls.login = LoginPage(browser)
        cls.index = IndexPage(browser)

    @classmethod
    def tearDownClass(cls) -> None:
        # 退出浏览器
        cls.login.quit()

    def tearDown(self) -> None:
        # 退出登录
        self.index.logout()

    @ddt.data(*account_list)
    def test_login(self, data):
        # print(data)
        # 请求目标网址
        self.login.get(self.login.login_url)
        # 登录的操作
        self.login.input_username((By.NAME, 'username'), data['username'])
        self.login.input_password((By.NAME, 'password'), data['password'])
        self.login.remember((By.ID, 'remember'))
        self.login.login((By.CLASS_NAME, 'us_Submit'))
        time.sleep(2)
        # 登录断言
        username = self.index.search_username()
        self.assertEqual(data['username'], username, msg='登录成功')


if __name__ == '__main__':
    unittest.main(verbosity=2)
