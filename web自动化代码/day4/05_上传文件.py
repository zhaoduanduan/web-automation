import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

# print(__file__)
# exit(0)
driver = webdriver.Chrome()
url = os.path.join(os.path.abspath('html'),'upload_file.html')
driver.get(url)
'''
# print(os.path.abspath(__file__)) # 当前文件的绝对路径
# print(os.path.dirname(os.path.abspath(__file__))) # 当前文件的绝对目录
# exit(0)
'''

# file = os.path.dirname(os.path.abspath(__file__)) + '/01_弹窗处理.py'
file = os.path.join(os.path.dirname(os.path.abspath(__file__)),'01_弹窗处理.py') # 这个也可以
time.sleep(2)
driver.find_element(By.NAME, 'file').send_keys(file)

time.sleep(2)
driver.quit()