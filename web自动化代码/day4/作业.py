import os
import time
from selenium import webdriver
from selenium.webdriver.common.by import By


class EmailOperation:
    def __init__(self):
        # 实例化配置类
        options = webdriver.ChromeOptions()
        # 准备配置项
        user_data_dir = r'--user-data-dir=C:\Users\戴先生\AppData\Local\Google\Chrome\User Data'
        # 添加配置项
        options.add_argument(user_data_dir)
        # 打开浏览器(设置options), 请求目标网址
        self.driver = webdriver.Chrome(options=options)
        self.driver.get('https://mail.163.com/')
        self.driver.maximize_window()

    def send(self):
        self.driver.find_element(By.XPATH,'//*[@id="_mail_component_149_149"]/span[2]').click()
        # print(self.driver.find_element(By.XPATH, '//*[@id="_mail_component_149_149"]/span[2]').get_attribute('outerHTML'))
        iframe1 = self.driver.find_element(By.CLASS_NAME, 'APP-editor-iframe')
        self.driver.switch_to.frame(iframe1)
        self.driver.find_element(By.CLASS_NAME, 'nui-scroll').send_keys('端端小仙女')
        # 切换到主页面
        self.driver.switch_to.default_content()
        # 输入收件人
        self.driver.find_element(By.CLASS_NAME, 'nui-editableAddr-ipt').send_keys('赵端端qq邮箱<331079446@qq.com>')
        # 输入主题
        self.driver.find_element(By.XPATH, '//input[contains(@id, "_subjectInput")]').send_keys('小猪佩奇')

        # 添加附件
        # iframe2 = self.driver.find_element(By.XPATH, '/html/body/div[2]/div[1]/div[2]/div[1]/section/header/div[3]/div[1]/iframe')
        # self.driver.switch_to.frame(iframe2)
        file = os.path.join(os.path.dirname(os.path.abspath(__file__)), '01_弹窗处理.py')
        self.driver.find_element(By.XPATH, '//input[@type="file"]').send_keys(file)
        time.sleep(3)
        # 点击发送
        self.driver.find_element(By.XPATH, '/html/body/div[2]/div[1]/div[2]/header/div/div[1]/div/span[2]').click()

    def quit(self):
        time.sleep(5)
        self.driver.quit()

if __name__ == '__main__':
   op = EmailOperation()
   op.send()
   op.quit()




