import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
url = os.path.join(os.path.abspath('html'), 'radio-checkbox.html')
driver.get(url)

inputs = driver.find_elements(By.XPATH, '//input[@type="checkbox"]')
for input in inputs:
    time.sleep(3)
    input.click()

time.sleep(2)
driver.quit()
