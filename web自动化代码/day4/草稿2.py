import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
url = os.path.join(os.path.abspath('html'), 'popup.html')
driver.get(url)

btn = driver.find_elements(By.TAG_NAME, 'button')
btn[0].click() # 点击alert
# 切换到弹窗,获取当前弹窗
alert = driver.switch_to.alert
time.sleep(2)
print(alert.text)# 打印出弹窗文本
alert.accept() # 确定

btn[1].click() # 点击confirm
# 切换到弹窗,获取当前弹窗
alert = driver.switch_to.alert
time.sleep(2)
print(alert.text)
alert.dismiss()


