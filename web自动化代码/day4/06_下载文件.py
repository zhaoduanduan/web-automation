
import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

# 实例化配置类
options = webdriver.ChromeOptions()
# 准备配置信息
'''
# 第一种方式  不推荐
download = os.path.dirname(os.path.abspath(__file__) + '\\download'  # 注意这里是两个反斜杠
# 第二种方式 
download = os.path.join(os.path.dirname(os.path.abspath(__file__)) ,'html') # 下载到html文件夹中
'''
download = os.path.dirname(os.path.abspath(__file__))  # 直接下载到day4文件夹中
prefs = {
    'profile.default_content_settings.popups':0, # 取消弹窗
    'download.default_directory':download # 设置保存位置
}
# 添加配置
options.add_experimental_option('prefs',prefs)
driver = webdriver.Chrome(options=options)

'''
driver.get('https://www.selenium.dev/downloads/')
driver.find_element(By.LINK_TEXT, '64 bit Windows IE').click()
'''

driver.get('https://www.cr173.com/soft/31043.html')
driver.find_element(By.LINK_TEXT, '湖北电信下载').click()

time.sleep(1000)
driver.quit()


