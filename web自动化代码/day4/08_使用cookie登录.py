import time

from selenium import webdriver

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.maximize_window()

time.sleep(3)

cookies = [
    {
        'name':'BDUSS_BFESS',
        'value':'zBGR2FxNzB0bjd3dGo3aFJweE42bX5FYjd4MHZ2V3pyTTN0SX5hLW5oSzFqdVJpRVFBQUFBJCQAAAAAAAAAAAEAAAAXTXmZRHVhbkR1YW5PT2xpZmUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALUBvWK1Ab1iR'
    },
    {
        'name':'BDUSS',
        'value':'zBGR2FxNzB0bjd3dGo3aFJweE42bX5FYjd4MHZ2V3pyTTN0SX5hLW5oSzFqdVJpRVFBQUFBJCQAAAAAAAAAAAEAAAAXTXmZRHVhbkR1YW5PT2xpZmUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALUBvWK1Ab1iR'
    }
]
# driver.add_cookie(cookies[0])
# driver.add_cookie(cookies[1])
for cookie in cookies:
    driver.add_cookie(cookie) # 遍历添加cookie
driver.get('https://www.baidu.com/')
# 刷新页面
driver.refresh()
time.sleep(5)
driver.quit()

