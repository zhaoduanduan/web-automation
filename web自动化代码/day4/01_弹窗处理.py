import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
url = os.path.join(os.path.abspath('html'), 'popup.html')
driver.get(url)

btn = driver.find_elements(By.TAG_NAME, "button")
btn[0].click()
# 切换到alert
alert = driver.switch_to.alert
time.sleep(2)
print(alert.text)
alert.accept()

# confirm
btn[1].click()
alert = driver.switch_to.alert
time.sleep(2)
print(alert.text)
alert.dismiss()
time.sleep(2)

# prompt
btn[2].click()
alert = driver.switch_to.alert
time.sleep(3)
alert.send_keys('abcdef')
print(alert.text)

time.sleep(2)
driver.quit()

