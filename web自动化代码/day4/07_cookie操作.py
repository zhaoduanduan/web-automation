from selenium import webdriver
from pprint import pprint

driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
'''
# TODO 获取所有cookie
cookies = driver.get_cookies()
pprint(cookies) # pprint格式化输出
'''
#  TODO 获取指定cookie  driver.get_cookie(name)
# pprint(driver.get_cookie('ZFY'))

# TODO 添加cookie  driver.add_cookie(cookie_dict)
dict = {'name':'hello','value':'12345'}
driver.add_cookie(dict)
pprint(driver.get_cookie('hello'))

# TODO 删除cookie
driver.delete_cookie('hello')
pprint(driver.get_cookie('hello'))

'''
# TODO 删除所有cookie(慎用)
driver.delete_all_cookies()
cookies = driver.get_cookies()
pprint(cookies)
'''

driver.quit()

