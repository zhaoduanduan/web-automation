import os.path
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
url = os.path.join(os.path.abspath('html'), 'radio-checkbox.html')
driver.get(url)

ele = driver.find_element(By.ID, 'boy')
ele.click()
print(ele.is_selected())
time.sleep(2)

girl = driver.find_element(By.ID, 'girl')
print(girl.is_selected())
girl.click()
time.sleep(2)
print(girl.is_selected())

time.sleep(3)
driver.quit()

